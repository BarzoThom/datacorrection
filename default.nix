{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/22.05.tar.gz";
    sha256 = "0d643wp3l77hv2pmg2fi7vyxn4rwy0iyr8djcw1h5x72315ck9ik";
  }) {}
}:

let
  self = rec {

        ctbikeCorr = pkgs.python3Packages.buildPythonApplication rec {
          pname = "ctbikeCorr";
          version = "0.2";

          src = pkgs.lib.sourceByRegex ./ctbikeCorr [
            ".*\.py"
          ];

      propagatedBuildInputs = with pkgs.python3Packages; [
            pandas
            matplotlib
            scipy
            numpy
            sphinx
          ];

      doCheck = false;

    };
};
in
    self
