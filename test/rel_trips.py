import unittest

import pandas as pd
from pandas.testing import assert_frame_equal

import ctbikeCorr.utils as ut
import ctbikeCorr.relative_trips as ret

class TestExample(unittest.TestCase):

    def test_examplefull(self):
        ret.compute_errors("test-data/exampleRelative/example-relocations.csv",
                           "test-data/exampleRelative/example-tripdata.csv",
                           "test-data/exampleRelative/example-stockdata.csv")
        res = pd.read_csv("test-data/exampleRelative/example-validity.csv")
        ref = pd.read_csv("test-data/exampleRelative/result_reference.csv")
        assert_frame_equal(ref, res, check_dtype=False)

unittest.main()
