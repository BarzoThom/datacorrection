import unittest

from pandas.testing import assert_frame_equal

import ctbikeCorr.reconstruct_stock as frel

class TestPlaceRelocations(unittest.TestCase):

    def test_72(self):
        trips_filename = "test-data/examplePlacing/trips72.csv"
        relocs_filename = "test-data/examplePlacing/relocs72.csv"
        stocks_filename = "test-data/examplePlacing/stock72.csv"
        res = frel.match_relocations(relocs_filename, stocks_filename, trips_filename)

unittest.main()
