import unittest

from pandas.testing import assert_frame_equal

import ctbikeCorr.find_relocation as frel
import ctbikeCorr.utils as ut



class TestSimple(unittest.TestCase):

    def test_singlebike(self):
        frel.get_relocation("test-data/singlebike/", 5)
        res = ut.read_tripfile("test-data/singlebike/201605-citibike-relocations-raw.csv")
        ref = ut.read_tripfile("test-data/singlebike/reloc-reference.csv")
        assert_frame_equal(ref, res, check_dtype=False)

    def test_crossedbike(self):
        frel.get_relocation("test-data/crossedbike/", 5)
        res = ut.read_tripfile("test-data/crossedbike/201605-citibike-relocations-raw.csv")
        ref = ut.read_tripfile("test-data/crossedbike/reloc-reference.csv")
        assert_frame_equal(ref, res, check_dtype=False)

class TestRefine(unittest.TestCase):

    def test_simplecase(self):
        relocs = ut.read_tripfile("test-data/refineSimple/201605-citibike-relocations-raw.csv")
        frel.refine_relocations(relocs, 5, 'test-data/refineSimple')
        res = ut.read_tripfile("test-data/refineSimple/201605-citibike-relocations.csv")
        ref = ut.read_tripfile("test-data/refineSimple/refined-reference.csv")
        assert_frame_equal(ref, res, check_dtype=False)

    def test_refineThree(self):
        relocs = ut.read_tripfile("test-data/refineThree/201605-citibike-relocations-raw.csv")
        frel.refine_relocations(relocs, 5, 'test-data/refineThree')
        res = ut.read_tripfile("test-data/refineThree/201605-citibike-relocations.csv")
        ref = ut.read_tripfile("test-data/refineThree/refined-reference.csv")
        assert_frame_equal(ref, res, check_dtype=False)

    def test_crossDayRefine(self):
        relocs = ut.read_tripfile("test-data/crossDayRefine/201605-citibike-relocations-raw.csv")
        frel.refine_relocations(relocs, 5, 'test-data/crossDayRefine/')
        res = ut.read_tripfile("test-data/crossDayRefine/201605-citibike-relocations.csv")
        ref = ut.read_tripfile("test-data/crossDayRefine/refined-reference.csv")
        assert_frame_equal(ref, res, check_dtype=False)

unittest.main()
