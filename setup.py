from setuptools import setup, find_packages

def readme():
    with open("README.md") as f:
        return f.read()

setup(name="ctbikeCorr",
      version="0.2",
      description="TODO",
      long_description=readme(),
      url="https://github.com/BarzolaT/TODO.git",
      author="barzolaT",
      author_email="thomas.barzola@gmail.com",
      license="MIT",
      packages=find_packages(where="src", exclude=("docs")),
      install_requires=["pandas", "numpy", "scipy", "matplotlib", "sphinx"],
      zip_safe=False
      )
