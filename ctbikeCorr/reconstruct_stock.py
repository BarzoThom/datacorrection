import os

import numpy as np
import pandas as pd

import ctbikeCorr.utils as ut

def unexplained_movements(stock_st, trips_st, station):
    """!
    Compute the movements that are still to explain from the stock when the trips are removed
    @param stock_st (stockdataframe): The stock concerned
    @param trips_st (tripdataframe): The trips concerned
    @param station (stationid): The identifier of the station concerned
    """
    explained = []
    for time, n_time in zip(stock_st["download_date"], stock_st["next_time"]):
        arr_trips = len(trips_st.loc[(trips_st["end station id"] == station ) &
                                     trips_st.stoptime.between(time, n_time)])
        dep_trips = len(trips_st.loc[(trips_st["start station id"] == station ) &
                                     trips_st.starttime.between(time, n_time)])
        explained.append(arr_trips-dep_trips)
    res = (stock_st["available_bikes"].diff() - np.array(explained)).astype('Int64')
    return res

def place_relocations(stock_st, relocs_st, station, treshold):
    """!
    For a single station and a same day we place the relocations
    @param stock_st (stockdataframe): The stock concerned
    @param relocs_st (tripdataframe): The relocations to place, must have the unexplained columns
    @param station (stationid): The identifier of the station concerned
    """
    sorted_stock = stock_st.sort_values(by="unexplained", key=abs, ascending=False)
    for _, row in sorted_stock.iterrows():
        if pd.isna(row["unexplained"]) or abs(row["unexplained"]) < treshold:
                break
        if row["unexplained"] < 0: # unexplained departures

            activ_rel = relocs_st.loc[relocs_st.start_unused &
                                      (relocs_st["start station id"] == station) &
                                      (relocs_st.starttime < row["next_time"]) &
                                      (relocs_st.stoptime > row["download_date"])].copy()
            activ_rel["time_diff"] = abs(activ_rel["starttime"] - row["download_date"])
            indexes = activ_rel.sort_values('time_diff').head(-1*row["unexplained"]).index
            relocs_st.loc[indexes, "starttime"] = row["download_date"]
            relocs_st.loc[indexes, "start_unused"] = False
        else:
            activ_rel = relocs_st.loc[relocs_st.end_unused &
                                      (relocs_st["end station id"] == station) &
                                      (relocs_st.starttime < row["next_time"]) &
                                      (relocs_st.stoptime > row["download_date"])].copy()
            activ_rel["time_diff"] = abs(activ_rel["stoptime"] - row["download_date"])
            indexes = activ_rel.sort_values('time_diff').head(row["unexplained"]).index
            relocs_st.loc[indexes, "stoptime"] = row["download_date"]
            relocs_st.loc[indexes, "end_unused"] = False
    return relocs_st

def match_relocations(reloc_filename, stock_filename, trip_filename, treshold=5):
    """!
    We place the relocations in time, trying to minimize the time of placement.
    @param reloc_filename (path-like): path to find the relocations to use, the name will be used for the result
    @param stock_filename (path-like): path to find the stock to use, the station in the stock will be used
    @param trip_filename (path-like): path to find the trips to use.

    @returns relocs (tripdataframe): placed relocations
    """
    relocs = ut.read_tripfile(reloc_filename)
    stock = ut.read_stockfile(stock_filename)
    trips = ut.read_tripfile(trip_filename)
    COLUMNS = relocs.columns
    #Preparing data
    stock["day"] = stock["download_date"].dt.floor("D")
    stock["next_day"] = stock["day"] + pd.DateOffset(days=1)
    stock["day"] = np.where(stock["download_date"].dt.hour < 12, stock["day"], stock['next_day'])
    trips["startday"] = trips["starttime"].dt.floor("D")
    trips["next_day"] = trips["startday"] + pd.DateOffset(days=1)
    trips["startday"] = np.where(trips["starttime"].dt.hour < 12, trips['startday'], trips['next_day'])
    trips["stopday"] = trips["stoptime"].dt.floor("D")
    trips["next_day"] = trips["stopday"] + pd.DateOffset(days=1)
    trips["stopday"] = np.where(trips["stoptime"].dt.hour < 12, trips['stopday'], trips['stopday'])
    relocs["startday"] = relocs["starttime"].dt.floor("D")
    relocs["next_day"] = relocs["startday"] + pd.DateOffset(days=1)
    relocs["startday"] = np.where(relocs["starttime"].dt.hour < 12, relocs['startday'], relocs['next_day'])
    relocs["stopday"] = relocs["stoptime"].dt.floor("D")
    relocs["next_day"] = relocs["stopday"] + pd.DateOffset(days=1)
    relocs["stopday"] = np.where(relocs["stoptime"].dt.hour < 12, relocs['stopday'], relocs['stopday'])
    relocs.drop(columns="next_day", inplace=True)
    relocs["start_unused"] = True
    relocs["end_unused"] = True
    for (station, day), stock_st in stock.groupby(["number", "day"]):
        stock_st["next_time"] = stock_st["download_date"].shift(-1)
        trips_st = trips[((trips["start station id"] == station)|(trips["end station id"] == station)) &
                         ((trips["startday"] == day) | (trips["stopday"] == day))]
        relocs_st = relocs[((relocs["end station id"] == station) |
                            (relocs["start station id"] == station)) &
                           ((relocs["startday"] == day) |
                            (relocs["stopday"] == day))].copy()
        stock_st["unexplained"] = unexplained_movements(stock_st, trips_st, station)
        relocs_st = place_relocations(stock_st, relocs_st, station, treshold)
        relocs.update(relocs_st)
    out_filename = reloc_filename.replace(".csv", f"-placed{treshold}.csv")
    relocs.filter(COLUMNS).to_csv(out_filename, index=False, date_format="%Y/%m/%d %H:%M:%S")
    return relocs

def construct_new_stock(reloc_filename, trip_filename, stock_filename, month=5):
    stock = ut.read_stockfile(stock_filename)
    COLUMNS = stock.columns
    stock = stock[(stock["status"] == "OPEN") & (stock["download_date"].dt.month == month)]
    relocs = ut.read_tripfile(reloc_filename)
    relocs = relocs[relocs["usertype"] == "relocation"]
    trips = ut.read_tripfile(trip_filename)
    all_trips = pd.concat([relocs, trips], axis=0)
    time = np.concatenate([all_trips["starttime"].values, all_trips["stoptime"].values], axis=0)
    value = [-1]*len(all_trips) + [1]*len(all_trips)
    station = np.concatenate([all_trips["start station id"].values, all_trips["end station id"].values])
    rel_stock = pd.DataFrame({"download_date":time, "value":value,"number": station})
    comp_stock = pd.merge(stock, rel_stock, how="outer").sort_values("download_date")
    comp_stock["value"] = comp_stock["value"].fillna(0)
    comp_stock["rel_stock"] = comp_stock.groupby("number")["value"].transform('cumsum')
    comp_stock["diff"] = comp_stock["available_bikes"] - comp_stock["rel_stock"]
    error = comp_stock.groupby("number").agg({"diff": np.median})
    new_stock = pd.merge(comp_stock, error, how="left", left_on='number', right_index=True)
    new_stock["available_bikes"] = new_stock["rel_stock"] + new_stock["diff_y"]
    new_stock["available_bikes"] = new_stock.groupby("number")["available_bikes"].transform(lambda x: x - min(0, x.min()))
    new_stock["bike_stands"] = new_stock.groupby("number")["bike_stands"].transform(lambda x: x.fillna(x.median()))
    new_stock["available_bike_stands"] = (new_stock["bike_stands"] - new_stock["available_bikes"]).clip(0, new_stock["bike_stands"])
    new_stock["conctract_name"] = "NewYork"
    new_stock["status"] = "OPEN"
    out_filename = stock_filename.replace(".csv", "-corrected.csv")
    new_stock.filter(COLUMNS).to_csv(out_filename, index=False, date_format="%Y/%m/%d %H:%M:%S")
    return new_stock
