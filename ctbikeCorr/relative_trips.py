import os

import pandas as pd

import ctbikeCorr.utils as ut


def compute_relative_trips(stock):
    rel_trips = stock["available_bikes"].diff()
    rel_arr = rel_trips[rel_trips > 0].sum()
    rel_dep = rel_trips[rel_trips < 0].sum()
    return -1*rel_dep, rel_arr


def get_errors(station, relocs, trips, stock):
    relocs_grp = relocs.groupby(["starttime", "stoptime"])
    valid_dep = 0
    valid_arr = 0
    error_dep = 0
    error_arr = 0
    for (start, stop), grp in relocs_grp:
        dep_relocs = len(relocs.loc[(relocs["start station id"] == station) &
                                    (relocs["starttime"] <= stop) &
                                    (relocs["stoptime"] >= start)])
        arr_relocs = len(relocs.loc[(relocs["end station id"] == station) &
                                    (relocs["starttime"] <= stop) &
                                    (relocs["stoptime"] >= start)])
        dep_trips = len(trips.loc[(trips["start station id"] == station) &
                                  trips["starttime"].between(start, stop)
                                 ])
        arr_trips = len(trips.loc[(trips["end station id"] == station) &
                                  trips["stoptime"].between(start, stop)
                                 ])
        rel_dep, rel_arr = compute_relative_trips(stock.loc[(stock["download_date"] >= start) &
                                                            (stock["download_date"] <= stop)])
        if dep_relocs + dep_trips >= rel_dep:
            valid_dep += len(grp)
        else:
            error_dep += len(grp)
        if arr_relocs + arr_trips >= rel_arr:
            valid_arr += len(grp)
        else:
            error_arr += len(grp)
    return valid_dep, valid_arr, error_dep, error_arr

def compute_errors(relocfilename, tripfilename, stockfilename):
    relocations = ut.read_tripfile(relocfilename)
    relocations = relocations[relocations["usertype"] == "relocation"]
    usr_trips = ut.read_tripfile(tripfilename)
    stocks = ut.read_stockfile(stockfilename)
    stocks = stocks[stocks["status"] == "OPEN"]
    stocks_grp = stocks.groupby("number")
    measure = []
    for station, stock in stocks_grp:
        relocs = relocations.loc[(relocations["start station id"] == station) |
                                 (relocations["end station id"] == station)]
        trips = usr_trips.loc[(usr_trips["start station id"] == station) |
                              (usr_trips["end station id"] == station)]
        errors = get_errors(station, relocs, trips, stock)
        measure.append([station] + list(errors))
    COLUMNS = ["number", "departure valid intervals", "arrival valid intervals", "departure invalid intervals", 'arrival invalid intervals']
    result = pd.DataFrame(measure, columns=COLUMNS)
    out_filename = os.path.join(os.path.dirname(relocfilename),
                                'measure',
                                os.path.basename(relocfilename).replace('relocations', 'validity'))
    result.to_csv(out_filename,index=False)
    return result

def count_movements(stockfilename, tripfilename, month, freq="M", relocfilename=None):
    usr_trips = ut.read_tripfile(tripfilename)
    stocks = ut.read_stockfile(stockfilename)
    stocks = stocks[stocks["status"] == "OPEN"]
    if freq != "M":
        stocks["freq"] = stocks["download_date"].dt.floor(freq=freq)
        usr_trips["start_freq"] = usr_trips["starttime"].dt.floor(freq=freq)
        usr_trips["stop_freq"] = usr_trips["stoptime"].dt.floor(freq=freq)
    else:
        # There is a fucking bug for floor with M frequency
        stocks["freq"] = stocks["download_date"].dt.to_period('M').dt.to_timestamp()
        usr_trips["start_freq"] = usr_trips["starttime"].dt.to_period('M').dt.to_timestamp()
        usr_trips["stop_freq"] = usr_trips["stoptime"].dt.to_period('M').dt.to_timestamp()
    usr_dep = usr_trips[usr_trips["starttime"].dt.month == month].groupby(["start station id", "start_freq"]).size()
    usr_arr = usr_trips[usr_trips["stoptime"].dt.month == month].groupby(["end station id", "stop_freq"]).size()
    result = pd.concat([usr_dep, usr_arr], axis=1).rename(columns = {0:"user departures", 1:"user arrivals"})
    if relocfilename:
        relocations = ut.read_tripfile(relocfilename)
        if freq != "M":
            relocations["start_freq"] = relocations["starttime"].dt.floor(freq=freq)
            relocations["stop_freq"] = usr_trips["stoptime"].dt.floor(freq=freq)
        else:
            # There is a fucking bug for floor with M frequency
            relocations["start_freq"] = relocations["starttime"].dt.to_period('M').dt.to_timestamp()
            relocations["stop_freq"] = relocations["stoptime"].dt.to_period('M').dt.to_timestamp()

        reloc_dep = relocations[relocations["starttime"].dt.month == month].groupby(["start station id", "start_freq"]).size()
        reloc_arr = relocations[relocations["stoptime"].dt.month == month].groupby(["end station id", "stop_freq"]).size()
        relocs = pd.concat([reloc_dep, reloc_arr], axis=1).rename(columns = {0:"relocation departures", 1:"relocation arrivals"})
        result = pd.merge(result, relocs, how="outer", left_index=True, right_index=True)
    res = []
    for (num, dl_freq), grp in stocks.groupby(["number","freq"]):
        rel_dep, rel_arr = compute_relative_trips(grp)
        res.append([num, dl_freq, rel_dep, rel_arr])
    rel_stock = pd.DataFrame(res, columns = ["number", "frequency", "relative departures", "relative arrivals"])
    result = pd.merge(result, rel_stock, how="outer", left_index=True, right_on=["number", "frequency"])
    out_filename = os.path.join(os.path.dirname(stockfilename),
                                'measure',
                                os.path.basename(stockfilename).replace('stockdata', f'movements-{freq}'))
    result.to_csv(out_filename, index=False)
    return result
