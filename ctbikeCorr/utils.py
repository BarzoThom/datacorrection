import os
import logging
import re
import glob

import pandas as pd

def correct_manually(line):
    """
    Correction added manually to some files
    """
    rules = [
        (',"}','}'),
        (',\"stA\[{','},{'),
    ]
    nw_line = line
    for idrule, rule in enumerate(rules):
        nw_line = re.sub(rule[0], rule[1], nw_line)
    return nw_line


def correct_line(idl, line):
    """
    Correct the line of an invalid json file, according to rules:
    - `[` must be preceded by `],` if not we remove `[`
    - `{` must be preceded by `,` if not we add `,`
    - `,{` must be preceded by `}` if not we add `}`
    - We add `"` if it is missing before words non key-words
    - string ending with `"` and numbers must be separated by `:`
    - string that are not followed by `}|:|,` are removed
    - `,` must be folloved by `{|"|[` if not the next character is removed
    - `},` must be followed by `{` if not they are removed

    @param line: (str) line to correct
    @returns nw_line: (str) corrected line according to rules
    @returns nb_sub: (str) number of correction made
    """
    rules = [
         ('([^\]].|.[^,])\[','\\1'),
         ('([^(,|[)]){', '\\1,{'),
         ('([^}]),{', '\\1},{'),
         ('({|,|:)(?!true|false|null)([A-z]+)','\\1"\\2'),
         ('(\w")([0-9])','\\1:\\2'),
         ('(\w")[^(}|,|:)]','\\1'),
         (':,', ':"",'),
         (',[^({|"|\[)]',','),
         ('},([^{])',',\\1'),
    ]
    nw_line = line
    nb_sub = 0
    for idrule, rule in enumerate(rules):
        nw_line, ns = re.subn(rule[0], rule[1], nw_line)
        nb_sub += ns
        if ns > 0:
            logging.debug(f"Corrected error on line: {idl} with rule {idrule}")
    return nw_line, nb_sub

def read_json_stockfile(filename):
    """
    Read stockfile trying to correct file errors

    @param filename: (path-like) filename to read
    """
    data = []
    nb_corr = 0
    man_corr = 0
    with open(filename) as f:
        json_data = f.readlines()
        for idl, d in enumerate(json_data):
            if d[:3] != '[{"':
                logging.warning(f"invalid line at line {idl}")
                logging.debug(d)
                continue
            try:
                data.append(pd.read_json(d, orient="records"))
            except ValueError:
                nw_line, nb_sub = correct_line(idl, d)
                nb_corr += nb_sub
                try:
                    data.append(pd.read_json(nw_line, orient="records"))
                except ValueError:
                    try:
                        nw_line = correct_manually(d)
                        data.append(pd.read_json(nw_line, orient="records"))
                        man_corr += 1
                        logging.debug(f"Correction made manually on {idl}")
                    except ValueError:
                        raise ValueError(f"Invalid line of line {idl}, no correction found")
    return pd.concat(data), nb_corr, man_corr

def convert_stockfile(filename):
    """
    Read stockfile and then saves it into a correctly named csv

    @param filename: (path-like) filename to read
    """
    data, nb_corr, man_corr = read_json_stockfile(filename)
    logging.warning(f"{nb_corr} automatic corrections made")
    logging.warning(f"{man_corr} manual corrections made")
    data["contract_name"] = "NewYork"
    # Some column names have errors so we regroup columns under status
    COLUMNS_misspell = {"status":["statusValue", "statusVValue", "tatusValue", "sttatusKey"],
                        "available_bike_stands": ["availableocks"],
                        "bike_stands":["totlDocks"],
                        "available_bikes":["availableBikees", "avaiableBikes"],
                       }
    nb_corr_col = 0
    for column, error_list in COLUMNS_misspell.items():
        for misspell in error_list:
            if misspell in data.columns:
                nb_corr_col += 1
                logging.debug(f"Wrong colum name {misspell} corrected into {column}")
                data[column].fillna(data[misspell], inplace=True)

    logging.warning(f"{nb_corr_col} columns corrections made")
    # Some values have errors in recording we then correct those errors
    nb_corr_value = 0
    OPEN_misspell = ["In Seervice", "InService", 'In Service', 'In Serrvice', "In servvice", "In Servvice"]
    CLOSED_misspell= ["Not n Service"]
    nb_corr_value += len(data[data["status"].isin(OPEN_misspell)])
    nb_corr_value += len(data[data["status"].isin(CLOSED_misspell)])
    data["status"].replace(to_replace=OPEN_misspell, value="OPEN", inplace=True)
    data["status"].replace(to_replace=CLOSED_misspell, value="Not In Service", inplace=True)
    logging.warning(f"{nb_corr_value} values corrections made")
    # We save the file
    data["download_date"] = pd.to_datetime(data['download_date'], unit='s', origin='unix')
    data["download_date"] -= pd.DateOffset(hours=6)
    timestamp = os.path.basename(filename).split("_")[-1].split("-")
    month=int(timestamp[1])-1
    ofilename = os.path.join(os.path.dirname(filename), f"{timestamp[0]}{month:02d}-citibike-stockdata.csv")
    COLUMNS = ["available_bike_stands","available_bikes", "bike_stands", "contract_name", "download_date", "number", "status"]
    data.filter(COLUMNS).to_csv(ofilename, index=False)

def read_tripfile(filename):
    trip = pd.read_csv(filename)
    trip["starttime"] = pd.to_datetime(trip["starttime"], format="%m/%d/%Y %H:%M:%S", errors="coerce").fillna(
                        pd.to_datetime(trip["starttime"], format="%Y/%m/%d %H:%M:%S", errors="coerce"))
    trip["stoptime"] = pd.to_datetime(trip["stoptime"], format="%m/%d/%Y %H:%M:%S", errors="coerce").fillna(
                        pd.to_datetime(trip["stoptime"], format="%Y/%m/%d %H:%M:%S", errors="coerce"))
    return trip

def read_all_tripfile(dirname):
    files = glob.glob(os.path.join(dirname, "*-tripdata.csv"))
    dfs = [read_tripfile(f) for f in files]
    trips = pd.concat(dfs , axis=0, ignore_index=True).sort_values("starttime")
    return trips

def read_stockfile(filename):
    stock = pd.read_csv(filename)
    stock["download_date"] = pd.to_datetime(stock["download_date"], format="%Y/%m/%d %H:%M:%S")
    return stock
