import os
import logging

import ctbikeCorr.utils as ut
import ctbikeCorr.find_relocation as fl
import ctbikeCorr.relative_trips as ret
import ctbikeCorr.reconstruct_stock as rest

logging.basicConfig(level=logging.DEBUG)
DATADIR = "../data"

def convert_all_files():
    filename = os.path.join(DATADIR, "data_all_NewYork.jjson_2016-05-01-1462076748")
    logging.info(f"Converting {filename}")
    ut.convert_stockfile(filename)
    filename = os.path.join(DATADIR, "data_all_NewYork.jjson_2016-06-01-1464755163")
    logging.info(f"Converting {filename}")
    ut.convert_stockfile(filename)
    filename = os.path.join(DATADIR, "data_all_NewYork.jjson_2016-07-01-1467347186")
    logging.info(f"Converting {filename}")
    ut.convert_stockfile(filename)

def construct_relocations():
    logging.info(f"Constructing relocations for {DATADIR}")
    relocs = fl.get_relocation(DATADIR, 5)

def refine_relocations():
    relocs = ut.read_tripfile(os.path.join(DATADIR, "201605-citibike-relocations-raw.csv"))
    logging.info(f"Refining relocations for {DATADIR}")
    fl.refine_relocations(relocs, 5, DATADIR)

def measure_intervals():
    reloc_filename = os.path.join(DATADIR, '201605-citibike-relocations-raw.csv')
    logging.info(f"Measuring interval validity for {reloc_filename}")
    ret.compute_errors(reloc_filename,
                       os.path.join(DATADIR, '201605-citibike-tripdata.csv'),
                       os.path.join(DATADIR, '201605-citibike-stockdata.csv'))
    reloc_filename = os.path.join(DATADIR, '201605-citibike-relocations.csv')
    logging.info(f"Measuring interval validity for {reloc_filename}")
    ret.compute_errors(reloc_filename,
                   os.path.join(DATADIR, '201605-citibike-tripdata.csv'),
                   os.path.join(DATADIR, '201605-citibike-stockdata.csv'))

def measure_movements():
    logging.info("Measuring movement for 2016-05 frequency Day")
    ret.count_movements(os.path.join(DATADIR, "201605-citibike-stockdata.csv"),
                        os.path.join(DATADIR, "201605-citibike-tripdata.csv"),
                        5,
                        freq="D",
                        relocfilename=os.path.join(DATADIR, "201605-citibike-relocations-raw.csv"))


def get_new_stock():
#    reloc_filename = os.path.join(DATADIR, "201605-citibike-relocations-raw.csv")
#    logging.info(f"Placing relocations for {reloc_filename}")
#    rest.match_relocations(reloc_filename,
#                           os.path.join(DATADIR, '201605-citibike-stockdata.csv'),
#                           os.path.join(DATADIR, "201605-citibike-tripdata.csv"))
    logging.info("Reconstructing new stock")
    rest.construct_new_stock(os.path.join(DATADIR, "201605-citibike-relocations-raw-placed5.csv"),
                             os.path.join(DATADIR, "201605-citibike-tripdata.csv"),
                             os.path.join(DATADIR, "201605-citibike-stockdata.csv"))

get_new_stock()
