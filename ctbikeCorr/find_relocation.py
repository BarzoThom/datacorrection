import os

import numpy as np
import pandas as pd

import ctbikeCorr.utils as ut

def get_relocation(dirname, month):
    """!
    We look for relocations according to the detailed method
    @param dirname (path-like): path to find the tripdata to use, all *-tripdata.csv will be used
    """
    data = ut.read_all_tripfile(dirname)
    COLUMNS = data.columns
    year = data["starttime"].dt.year.values[0]
    grp_data = data.sort_values("starttime").groupby("bikeid", as_index=False)
    data["prev_position"] = grp_data['end station id'].shift()
    data["next_position"] = grp_data['start station id'].shift(-1)
    data["prev_time"] = grp_data['stoptime'].shift()
    data["lat_prev"] = grp_data['end station latitude'].shift()
    data["lon_prev"] = grp_data['end station longitude'].shift()
    data["prev_station_name"] = grp_data['end station name'].shift()
    #Get Relocations
    relocs = data.loc[data["prev_position"].notna() &
                      (data["prev_position"] != data["start station id"])].copy()
    relocs['end station name'] = relocs["start station name"]
    relocs['end station id'] = relocs["start station id"]
    relocs['end station latitude'] = relocs["start station latitude"]
    relocs['end station longitude'] = relocs["start station longitude"]
    relocs['usertype'] = "relocation"
    relocs['birth year'] = None
    relocs["gender"] = None
    relocs['stoptime'] = relocs["starttime"]
    relocs['start station name'] = relocs["prev_station_name"]
    relocs['start station id'] = relocs["prev_position"]
    relocs['start station latitude'] = relocs["lat_prev"]
    relocs['start station longitude'] = relocs["lon_prev"]
    relocs['starttime'] = relocs["prev_time"]
    relocs['tripduration'] = (relocs["stoptime"] - relocs["starttime"]).dt.total_seconds()
    relocs = relocs.loc[(relocs["starttime"].dt.month==month) |
                        (relocs["stoptime"].dt.month==month)].copy()
    #Get Movements
    movements = data.loc[(data["stoptime"].dt.month == month) &
                          data["next_position"].isnull()].copy()
    movements["starttime"] = movements["stoptime"]
    movements["start station name"] = movements["end station name"]
    movements["start station id"] = movements["end station id"]
    movements["start station latitude"] = movements["end station latitude"]
    movements["start station longitude"] = movements["end station longitude"]
    movements['usertype'] = "movements"
    movements['birth year'] = None
    movements["gender"] = None
    movements['stoptime'] = None
    movements['tripduration'] = None
    #Merging results
    relocations = pd.concat([relocs, movements], axis=0).\
                  filter(COLUMNS).\
                  sort_values("starttime")
    out_filename = os.path.join(dirname, f"{year}{month:02d}-citibike-relocations-raw.csv")
    relocations.to_csv(out_filename, index=False, date_format="%Y/%m/%d %H:%M:%S")
    return relocations

def new_starttime(group):
    def _find_starttime(x):
        valid = group.starttime[group["starttime"] < x]
        return max(valid)
    group["nw_starttime"] = group["stoptime"].apply(_find_starttime)
    nw_array = group["nw_starttime"].values
    for i in range(1, len(nw_array)):
        nw_array[i] = max(nw_array[i-1], nw_array[i])
    group["nw_starttime"] = nw_array
    return group

def new_stoptime(group):
    def _find_stoptime(x):
        valid = group.stoptime[group["stoptime"] > x]
        return min(valid)
    group["nw_stoptime"] = group["starttime"].apply(_find_stoptime)
    nw_array = group["nw_stoptime"].values
    for i in range(1, len(nw_array)):
        nw_array[i] = max(nw_array[i-1], nw_array[i])
    group["nw_stoptime"] = nw_array
    return group

def refine_relocations(relocs, month, dirname="../data"):
    COLUMNS = relocs.columns
    movements = relocs[relocs["usertype"] == "movements"].copy()
    relocs = relocs[relocs["usertype"] == "relocation"].copy()
    relocs["stoptime"] = pd.to_datetime(relocs["stoptime"])
    relocs["start_day"] = relocs["starttime"].dt.floor(freq="D")
    relocs.loc[relocs["starttime"].dt.hour >= 12, "start_day"] += pd.DateOffset(hours=12)
    relocs["stop_day"] = relocs["stoptime"].dt.floor(freq="D")
    relocs.loc[relocs["stoptime"].dt.hour >= 12, "stop_day"] += pd.DateOffset(hours=12)
    relocs = relocs.groupby(["start station id", "start_day"]).apply(new_starttime)
    relocs = relocs.groupby(["end station id", "stop_day"]).apply(new_stoptime)
    relocs['starttime'] = relocs["nw_starttime"]
    relocs['stoptime'] = relocs["nw_stoptime"]
    relocs['tripduration'] = (relocs["stoptime"] - relocs["starttime"]).dt.total_seconds()
    relocs.drop(columns=["nw_starttime", "nw_stoptime", "start_day", "stop_day"], inplace=True)
    relocation = pd.concat([movements, relocs], axis=0).sort_values("starttime")
    year = relocs["starttime"].dt.year.values[0]
    out_filename = os.path.join(dirname, f"{year}{month:02d}-citibike-relocations.csv")
    relocation.filter(COLUMNS).to_csv(out_filename, index=False, date_format="%Y/%m/%d %H:%M:%S")
    return relocation
