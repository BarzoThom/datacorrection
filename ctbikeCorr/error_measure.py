import os

import pandas as pd

import utils as ut
def prepare_data(year, month, datadir="../data"):
    tripfilename = os.path.join(datadir, f"{year}{month:02d}-citibike-tripdata.csv")
    trips = ut.read_tripfile(tripfilename)
    relocfilename = os.path.join(datadir, f"{year}{month:02d}-citibike-relocations.csv")
    relocs = ut.read_tripfile(relocfilename)
    stockfilename = os.path.join(datadir, f"{year}{month:02d}-citbike-stockdata.csv")
    stock = ut.read_tripfile(stockfilename)
    return trips, relocs, stock

def earliest_relative_stock(trips, relocs):
    relocs = relocs.copy()
    relocs["stoptime"] = relocs["starttime"]
    all_trips = pd.concat([relocs, trips], axis=0, ignore_index=True)
    early_stock = pd.dataframe({"time": all_trips["starttime"].append(all_trips["stoptime"]),
                                "station": all_trips["start station id"].append(all_trips["end station id"]),
                                "value": [1]*len(all_trips)+[-1]*len(all_trips)})
    early_stock = early_stock.loc[early_stock["time"].dt.month == 5,:].copy()
    early_stock["stock"] = early_stock.group_by("time")["value"].cumsum()
    return early_stock

def latest_relative_stock(trips, relocs):
    relocs = relocs.copy()
    relocs["starttime"] = relocs["stoptime"]
    all_trips = pd.concat([relocs, trips], axis=0, ignore_index=True)
    late_stock = pd.dataframe({"time": all_trips["starttime"].append(all_trips["stoptime"]),
                                "station": all_trips["start station id"].append(all_trips["end station id"]),
                                "value": [1]*len(all_trips)+[-1]*len(all_trips)})
    late_stock = late_stock.loc[late_stock["time"].dt.month == 5,:].copy()
    late_stock["stock"] = late_stock.group_by("time")["value"].cumsum()
    return late_stock

def error_measure(year, month, datadir="../data"):
    trips, relocs, stock = prepare_data(year, month, datadir)
    early_stock = earliest_relative_stock(trips, relocs)
    late_stock = latest_relative_stock(trips, relocs)
